package detect

import (
	"io"
	"strings"
	"time"

	"github.com/meskio/epubgo"

	"0xacab.org/ale/ebookmeta/meta"
)

type epubReader struct {
	spine *epubgo.SpineIterator
	cur   io.ReadCloser
}

func (r *epubReader) Close() (err error) {
	if r.cur != nil {
		err = r.cur.Close()
	}
	return
}

func (r *epubReader) Read(b []byte) (int, error) {
	for {
		if r.cur == nil {
			err := r.spine.Next()
			if err != nil {
				return 0, io.EOF
			}
			r.cur, err = r.spine.Open()
			if err != nil {
				return 0, err
			}
		}

		n, err := r.cur.Read(b)
		if err == io.EOF {
			r.cur.Close() // nolint
			r.cur = nil
			err = nil
		}
		if n > 0 {
			return n, err
		}
	}
}

func newEpubReader(epub *epubgo.Epub) (*epubReader, error) {
	spine, err := epub.Spine()
	if err != nil {
		return nil, err
	}
	return &epubReader{spine: spine}, nil
}

type epubDoc struct {
	path string
	epub *epubgo.Epub
}

func newEpub(path string) (*epubDoc, error) {
	e, err := epubgo.Open(path)
	if err != nil {
		return nil, err
	}
	return &epubDoc{path: path, epub: e}, nil
}

func (e *epubDoc) Close() {
	e.epub.Close()
}

func (e *epubDoc) Reader() (io.ReadCloser, error) {
	r, err := newEpubReader(e.epub)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func parseIdentifier(data string, attrs map[string]string) (meta.Identifier, bool) {
	if attrs["scheme"] == "ISBN" {
		return meta.Identifier{Type: meta.IdTypeISBN, Value: data}, true
	}
	return meta.Identifier{}, false
}

func (e *epubDoc) Metadata() (*meta.Metadata, error) {
	var m meta.Metadata

	for _, field := range e.epub.MetadataFields() {
		attrs, err := e.epub.MetadataAttr(field)
		if err != nil {
			continue
		}
		data, err := e.epub.Metadata(field)
		if err != nil {
			continue
		}
		for i := range data {
			if data[i] == "" {
				continue
			}

			switch field {
			case "title":
				m.Title = data[i]
			case "subtitle":
				m.Subtitle = data[i]
			case "language":
				m.Language = strings.ToLower(data[i])
			case "description":
				m.Description = data[i]
			case "creator":
				m.Author = append(m.Author, data[i])
			case "subject":
				m.Subject = append(m.Subject, data[i])
			case "publisher":
				m.Publisher = data[i]
			case "date":
				m.PublishedDate = meta.ParseDate(data[i])
			case "identifier":
				id, ok := parseIdentifier(data[i], attrs[i])
				if ok {
					m.IDs = append(m.IDs, id)
				}
			}
		}
	}

	m.Source = []meta.Source{
		{
			Name:      "epub",
			URL:       "file://" + e.path,
			Quality:   meta.SourceQualityFile,
			Timestamp: time.Now(),
		},
	}

	return m.Normalize(), nil
}
