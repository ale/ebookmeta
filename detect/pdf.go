package detect

import (
	"bytes"
	"io"
	"io/ioutil"
	"os/exec"
	"regexp"
	"time"

	"0xacab.org/ale/ebookmeta/meta"
)

type pdfDoc struct {
	path string
}

func newPDF(path string) (*pdfDoc, error) {
	return &pdfDoc{path: path}, nil
}

func (p *pdfDoc) Close() {}

func (p *pdfDoc) Reader() (io.ReadCloser, error) {
	data, err := exec.Command(
		"pdftotext",
		"-q",
		"-nopgbrk",
		"-enc", "UTF-8",
		p.path,
		"-",
	).Output()
	if err != nil {
		return nil, err
	}
	return ioutil.NopCloser(bytes.NewReader(data)), nil
}

var (
	htmlTitleRx = regexp.MustCompile(`<title>([^<]*)</title>`)
	htmlMetaRx  = regexp.MustCompile(`<meta name="([^"]*)" content="([^"]*)"`)
)

func (p *pdfDoc) Metadata() (*meta.Metadata, error) {
	data, err := exec.Command(
		"pdftotext",
		"-q",
		"-enc", "UTF-8",
		"-htmlmeta",
		"-f", "1", "-l", "1",
		p.path,
		"-",
	).Output()
	if err != nil {
		return nil, err
	}

	var m meta.Metadata
	for _, matches := range htmlMetaRx.FindAllSubmatch(data, -1) {
		key := string(matches[1])
		value := string(matches[2])
		switch key {
		case "Author":
			m.Author = append(m.Author, value)
		case "CreationDate":
			m.PublishedDate = meta.ParseDate(value)
		}
	}
	matches := htmlTitleRx.FindSubmatch(data)
	if len(matches) > 0 {
		m.Title = string(matches[1])
	}

	m.Source = []meta.Source{
		{
			Name:      "pdf",
			URL:       "file://" + p.path,
			Quality:   meta.SourceQualityFile,
			Timestamp: time.Now(),
		},
	}

	return m.Normalize(), nil
}
