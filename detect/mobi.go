package detect

import (
	"bytes"
	"io"
	"io/ioutil"
	"time"

	"0xacab.org/ale/ebookmeta/meta"
	"github.com/clee/gobipocket"
)

type mobiDoc struct {
	mobi *mobipocket.Mobipocket
	path string
}

func newMobi(path string) (*mobiDoc, error) {
	m, err := mobipocket.Open(path)
	if err != nil {
		return nil, err
	}
	return &mobiDoc{mobi: m, path: path}, nil
}

func (d *mobiDoc) Metadata() (*meta.Metadata, error) {
	var m meta.Metadata

	for key, values := range d.mobi.Metadata {
		for _, value := range values {
			switch key {
			case "isbn":
				m.IDs = append(m.IDs, meta.Identifier{Type: meta.IdTypeISBN, Value: value})
			case "title":
				m.Title = value
			case "publisher":
				m.Publisher = value
			case "pubdate":
				m.PublishedDate = meta.ParseDate(value)
			case "author":
				m.Author = append(m.Author, value)
			case "description":
				m.Description = value
			case "subject":
				m.Subject = append(m.Subject, value)
			}
		}
	}

	m.Source = []meta.Source{
		{
			Name:      "mobi",
			URL:       "file://" + d.path,
			Quality:   meta.SourceQualityFile,
			Timestamp: time.Now(),
		},
	}

	return m.Normalize(), nil
}

func (d *mobiDoc) Reader() (io.ReadCloser, error) {
	var readers []io.Reader
	for _, data := range d.mobi.RawTextRecords {
		readers = append(readers, bytes.NewReader(data))
	}
	return ioutil.NopCloser(io.MultiReader(readers...)), nil
}

func (d *mobiDoc) Close() {
	//d.mobi.Close()
}
