package detect

import (
	"errors"
	"io"
	"log"
	"path/filepath"
	"strings"

	"0xacab.org/ale/ebookmeta/isbndetect"
	"0xacab.org/ale/ebookmeta/meta"
)

type doc interface {
	Reader() (io.ReadCloser, error)
	Metadata() (*meta.Metadata, error)
	Close()
}

func openDoc(path string) (d doc, err error) {
	switch strings.ToLower(filepath.Ext(path)) {
	case ".epub", ".epub3":
		d, err = newEpub(path)
	case ".mobi":
		d, err = newMobi(path)
	case ".pdf":
		d, err = newPDF(path)
	default:
		err = errors.New("unsupported file format")
	}
	return
}

func ParseMetadataFromFile(path string) (*meta.Metadata, error) {
	d, err := openDoc(path)
	if err != nil {
		return nil, err
	}
	defer d.Close()

	m, err := d.Metadata()
	if err != nil {
		return nil, err
	}

	// Attempt to find ISBNs in the book text.
	dr, err := d.Reader()
	if err != nil {
		return nil, err
	}
	defer dr.Close()
	ids := isbndetect.FindISBN(dr)
	if len(ids) > 0 {
		log.Printf("autodetected ISBN(s): %v", ids)
		for _, isbn := range ids {
			m.IDs = append(m.IDs, meta.Identifier{Type: meta.IdTypeISBN, Value: isbn})
		}
		m.IDs = meta.UniqueIdentifiers(m.IDs)
	}

	return m, nil
}
