package meta

import "time"

var (
	canonicalDateFormat  = "2006-01-02"
	supportedDateFormats = []string{
		time.RFC3339,
		time.RFC822,
		"2006/01/02",
		"2006-01-02",
		"2006",
	}
)

// ParseDate parses a date and returns it in canonical format (Y-m-d).
func ParseDate(s string) string {
	for _, fmt := range supportedDateFormats {
		if t, err := time.Parse(fmt, s); err == nil {
			return t.Format(canonicalDateFormat)
		}
	}
	return ""
}
