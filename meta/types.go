package meta

import (
	"strings"
	"time"

	"golang.org/x/text/unicode/norm"
)

const (
	SourceQualityFile        = "file"
	SourceQualityUserCatalog = "user"
	SourceQualitySearch      = "search"
	SourceQualityISBN        = "isbn_lookup"
)

type SourceQuality string

// Relative priorities of SourceQuality enum. Leave 0 unused.
var sourceQualityOrder = map[SourceQuality]int{
	SourceQualityFile:        1,
	SourceQualityUserCatalog: 2,
	SourceQualitySearch:      3,
	SourceQualityISBN:        4,
}

func (q SourceQuality) LessThan(b SourceQuality) bool {
	aq := sourceQualityOrder[q]
	bq := sourceQualityOrder[b]
	return aq < bq
}

type Source struct {
	Quality   SourceQuality `json:"quality"`
	Name      string        `json:"name"`
	URL       string        `json:"url"`
	Timestamp time.Time     `json:"timestamp"`
}

type Metadata struct {
	IDs []Identifier `json:"ids"`

	Source []Source `json:"source,omitempty"`

	Title    string   `json:"title"`
	Subtitle string   `json:"subtitle,omitempty"`
	Author   []string `json:"author,omitempty"`
	Language string   `json:"language"`
	Format   []string `json:"format,omitempty"`
	NumPages int      `json:"num_pages"`

	Publisher         string `json:"publisher,omitempty"`
	PublishedDate     string `json:"published_date,omitempty"`
	PublishedLocation string `json:"published_location,omitempty"`

	Description string   `json:"description,omitempty"`
	Notes       string   `json:"notes,omitempty"`
	Subject     []string `json:"subject,omitempty"`
}

func nfcNormList(l []string) []string {
	out := make([]string, 0, len(l))
	for _, s := range l {
		out = append(out, norm.NFC.String(s))
	}
	return out
}

// Normalize metadata values that have semantic constraints. Returns a
// new object, does not modify original.
func (m *Metadata) Normalize() *Metadata {
	out := *m

	out.IDs = UniqueIdentifiers(m.IDs)

	out.Title = norm.NFC.String(m.Title)
	out.Subtitle = norm.NFC.String(m.Subtitle)
	out.Publisher = norm.NFC.String(m.Publisher)
	out.Author = nfcNormList(m.Author)

	out.Language = strings.ToLower(m.Language)
	if len(out.Language) > 2 {
		out.Language = out.Language[:2]
	}

	return &out
}

func (m *Metadata) highestSourceQuality() SourceQuality {
	var q SourceQuality
	for _, src := range m.Source {
		if q.LessThan(src.Quality) {
			q = src.Quality
		}
	}
	return q
}

func betterSource(a, b *Metadata) *Metadata {
	aq := a.highestSourceQuality()
	bq := b.highestSourceQuality()
	if bq > aq {
		return b
	}
	return a
}

func firstNonEmpty(a, b string) string {
	if a != "" {
		return a
	}
	return b
}

func firstNonEmptyList(a, b []string) []string {
	if len(a) > 0 {
		return a
	}
	return b
}

// Merge updates a Metadata object with refined information from
// another object. The results depend on the respective sources'
// qualities. Returns a new object, does not modify original value.
func (m *Metadata) Merge(other *Metadata) *Metadata {
	better := betterSource(m, other)

	var out Metadata
	out.Title = firstNonEmpty(better.Title, m.Title)
	out.Subtitle = firstNonEmpty(better.Subtitle, m.Subtitle)
	out.Author = firstNonEmptyList(better.Author, m.Author)
	out.Language = firstNonEmpty(better.Language, m.Language)
	out.Format = firstNonEmptyList(better.Format, m.Format)
	out.Publisher = firstNonEmpty(better.Publisher, m.Publisher)
	out.PublishedDate = firstNonEmpty(better.PublishedDate, m.PublishedDate)
	out.PublishedLocation = firstNonEmpty(better.PublishedLocation, m.PublishedLocation)
	out.Description = firstNonEmpty(better.Description, m.Description)
	out.Notes = firstNonEmpty(better.Notes, m.Notes)
	out.Subject = firstNonEmptyList(better.Subject, m.Subject)

	if better.NumPages > 0 {
		out.NumPages = better.NumPages
	}

	out.IDs = better.IDs
	out.Source = append(m.Source, better.Source...)

	return &out
}
