package meta

import (
	"fmt"
	"strings"
)

const (
	IdTypeISBN   = "isbn"
	IdTypeASIN   = "asin"
	IdTypeUUID   = "uuid"
	IdTypeGoogle = "google"
)

type Identifier struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

func (i Identifier) String() string {
	return fmt.Sprintf("%s:%s", i.Type, i.Value)
}

// ParseIdentifier parses a type:value string identifier.
func ParseIdentifier(s, defaultType string) (Identifier, error) {
	n := strings.IndexByte(s, ':')
	if n < 0 {
		if defaultType == "" {
			defaultType = IdTypeUUID
		}
		return Identifier{Type: defaultType, Value: s}, nil
	}
	return Identifier{
		Type:  strings.ToLower(s[:n]),
		Value: s[n+1:],
	}, nil
}

// UniqueIdentifiers returns a unique list of identifiers, removing repetitions.
func UniqueIdentifiers(l []Identifier) []Identifier {
	tmp := make(map[Identifier]struct{})
	for _, id := range l {
		tmp[id] = struct{}{}
	}
	out := make([]Identifier, 0, len(tmp))
	for id := range tmp {
		out = append(out, id)
	}
	return out
}
