package lookup

import (
	"context"
	"errors"

	"0xacab.org/ale/ebookmeta/lookup/google"
	"0xacab.org/ale/ebookmeta/lookup/openlibrary"
	"0xacab.org/ale/ebookmeta/meta"
)

type Refiner interface {
	Lookup(context.Context, []meta.Identifier) ([]*meta.Metadata, error)
	Similar(context.Context, *meta.Metadata) ([]*meta.Metadata, error)
}

func New(backend string) (Refiner, error) {
	switch backend {
	case "google":
		return google.New(), nil
	case "ol", "openlibrary":
		return openlibrary.New(), nil
	default:
		return nil, errors.New("not implemented")
	}
}
