package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"0xacab.org/ale/ebookmeta/lookup"
	"0xacab.org/ale/ebookmeta/meta"
)

var (
	be     = flag.String("backend", "google", "which backend to use")
	author = flag.String("author", "", "author info")
	title  = flag.String("title", "", "title")
)

func main() {
	flag.Parse()

	if *author == "" && *title == "" {
		log.Fatalf("Use --author and --title")
	}

	var m meta.Metadata
	m.Title = *title
	if *author != "" {
		m.Author = append(m.Author, *author)
	}

	ref, err := lookup.New(*be)
	if err != nil {
		log.Fatal(err)
	}

	results, err := ref.Similar(context.Background(), &m)
	if err != nil {
		log.Fatal(err)
	}
	for _, r := range results {
		d, _ := json.MarshalIndent(r, "", "  ")
		fmt.Printf("%s\n", string(d))
	}

	mm, ok := lookup.SelectWithConfidence(&m, results)
	if ok {
		d, _ := json.MarshalIndent(mm, "", "  ")
		fmt.Printf("\n\nselected result:\n%s\n", string(d))
	}
}
