package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"0xacab.org/ale/ebookmeta/lookup"
	"0xacab.org/ale/ebookmeta/meta"
)

var be = flag.String("backend", "amazon", "which backend to use")

func main() {
	flag.Parse()

	var isbns []meta.Identifier
	for _, arg := range flag.Args() {
		isbns = append(isbns, meta.Identifier{Type: meta.IdTypeISBN, Value: arg})
	}
	log.Printf("looking up isbns: %v", isbns)

	ref, err := lookup.New(*be)
	if err != nil {
		log.Fatal(err)
	}

	results, err := ref.Lookup(context.Background(), isbns)
	if err != nil {
		log.Fatal(err)
	}
	for _, r := range results {
		d, _ := json.MarshalIndent(r, "", "  ")
		fmt.Printf("%s\n", string(d))
	}
}
