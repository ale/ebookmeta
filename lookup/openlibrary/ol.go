package openlibrary

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"0xacab.org/ale/ebookmeta/lookup/httpclient"
	"0xacab.org/ale/ebookmeta/meta"
)

const openLibrarySourceName = "openlibrary"

type olName struct {
	Name string `json:"name"`
}

type olExcerpt struct {
	Text string `json:"text"`
}

type olSubject struct {
	URL  string `json:"url"`
	Name string `json:"name"`
}

type olMetadata struct {
	Key           string              `json:"key"`
	Title         string              `json:"title"`
	Subtitle      string              `json:"subtitle"`
	Notes         string              `json:"notes"`
	Identifiers   map[string][]string `json:"identifiers"`
	Authors       []*olName           `json:"authors"`
	Excerpts      []*olExcerpt        `json:"excerpts"`
	Publishers    []*olName           `json:"publishers"`
	PublishPlaces []*olName           `json:"publish_places"`
	PublishDate   string              `json:"publish_date"`
	Cover         map[string]string   `json:"cover"`
	Language      []string
	NumPages      int          `json:"number_of_pages"`
	Subjects      []*olSubject `json:"subjects"`
	URL           string       `json:"url"`
}

type olSearchResults struct {
	Docs []olSearchResult `json:"docs"`
}

type olSearchResult struct {
	Title string   `json:"title"`
	OLKey string   `json:"key"`
	Seeds []string `json:"seed"`
}

var dateRx = regexp.MustCompile(`(\d{4})`)

func (om *olMetadata) toMetadata(sourceQuality string) *meta.Metadata {
	var m meta.Metadata

	// Simple fields.
	m.Title = om.Title
	m.Subtitle = om.Subtitle
	m.Notes = om.Notes
	m.NumPages = om.NumPages
	for _, a := range om.Authors {
		m.Author = append(m.Author, a.Name)
	}
	for _, s := range om.Subjects {
		m.Subject = append(m.Subject, s.Name)
	}
	if len(om.Language) > 0 {
		m.Language = om.Language[0]
	}
	if len(om.Publishers) > 0 {
		m.Publisher = om.Publishers[0].Name
	}
	if len(om.PublishPlaces) > 0 {
		m.PublishedLocation = om.PublishPlaces[0].Name
	}
	if len(om.Excerpts) > 0 {
		m.Description = om.Excerpts[0].Text
	}

	// Extract identifiers.
	for key, values := range om.Identifiers {
		for _, value := range values {
			var id meta.Identifier
			id.Value = value
			switch key {
			case "isbn_10", "isbn_13":
				id.Type = meta.IdTypeISBN
			default:
				id.Type = key
			}
			m.IDs = append(m.IDs, id)
		}
	}

	if match := dateRx.FindString(om.PublishDate); match != "" {
		m.PublishedDate = meta.ParseDate(match)
	}

	// Source information.
	m.Source = []meta.Source{
		{
			Name:      openLibrarySourceName,
			URL:       om.URL,
			Quality:   meta.SourceQuality(sourceQuality),
			Timestamp: time.Now(),
		},
	}

	return m.Normalize()
}

type OpenLibrary struct {
	client httpclient.Client
}

func New() *OpenLibrary {
	return &OpenLibrary{
		client: httpclient.New(3),
	}
}

func (ol *OpenLibrary) doRequest(ctx context.Context, uri string, out interface{}) error {
	resp, err := ol.client.Get(ctx, uri)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(out)
}

// Return book information given an ID.
func (ol *OpenLibrary) queryBibKeys(ctx context.Context, bibkeys []string, sourceQuality string) ([]*meta.Metadata, error) {
	values := make(url.Values)
	values.Set("bibkeys", strings.Join(bibkeys, ","))
	values.Set("jscmd", "data")
	values.Set("format", "json")
	uri := fmt.Sprintf("https://openlibrary.org/api/books?%s", values.Encode())

	var result map[string]*olMetadata
	if err := ol.doRequest(ctx, uri, &result); err != nil {
		return nil, err
	}

	var out []*meta.Metadata
	for _, m := range result {
		out = append(out, m.toMetadata(sourceQuality))
	}
	return out, nil
}

func (ol *OpenLibrary) Lookup(ctx context.Context, identifiers []meta.Identifier) ([]*meta.Metadata, error) {
	var bibkeys []string

	for _, id := range identifiers {
		if id.Type == meta.IdTypeISBN {
			bibkeys = append(bibkeys, fmt.Sprintf("ISBN:%s", id.Value))
		}
	}
	if len(bibkeys) == 0 {
		return nil, errors.New("no ISBN identifiers found")
	}

	return ol.queryBibKeys(ctx, bibkeys, meta.SourceQualityISBN)
}

func (ol *OpenLibrary) Similar(ctx context.Context, m *meta.Metadata) ([]*meta.Metadata, error) {
	values := make(url.Values)
	if m.Title != "" {
		values.Set("title", m.Title)
	}
	for _, a := range m.Author {
		values.Add("author", a)
	}
	if len(values) == 0 {
		return nil, errors.New("no metadata to query")
	}

	var result olSearchResults
	uri := fmt.Sprintf("https://openlibrary.org/search.json?%s", values.Encode())
	if err := ol.doRequest(ctx, uri, &result); err != nil {
		return nil, err
	}

	var bibkeys []string
	for _, doc := range result.Docs {
		if len(doc.Seeds) == 0 {
			continue
		}
		// Just follow the first seed that hopefully points at a book.
		seed := doc.Seeds[0]
		bibkeys = append(bibkeys, fmt.Sprintf("OLID:%s", seed[7:]))
	}

	return ol.queryBibKeys(ctx, bibkeys, meta.SourceQualitySearch)
}
