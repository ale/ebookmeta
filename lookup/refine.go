package lookup

import (
	"context"
	"errors"
	"log"
	"sort"
	"strings"

	"0xacab.org/ale/ebookmeta/meta"
	"github.com/agnivade/levenshtein"
)

const editDistanceThreshold = 10

type metaWithDistance struct {
	meta           *meta.Metadata
	distance       int
	authorDistance int
	titleDistance  int
}

func swapAuthorTitle(m *meta.Metadata) *meta.Metadata {
	sm := *m
	sm.Author = []string{m.Title}
	if len(m.Author) > 0 {
		sm.Title = m.Author[0]
	} else {
		sm.Title = ""
	}
	return &sm
}

func min(args ...int) int {
	m := args[0]
	for i := 1; i < len(args); i++ {
		if args[i] < m {
			m = args[i]
		}
	}
	return m
}

func titleDistance(a, b *meta.Metadata) int {
	return levenshtein.ComputeDistance(
		strings.ToLower(a.Title),
		strings.ToLower(b.Title),
	)
}

func orEmptyStringList(l []string) []string {
	if len(l) == 0 {
		return []string{""}
	}
	return l
}

func authorDistance(a, b *meta.Metadata) int {
	// Minimum distance of every author<->author pair. If either
	// side has zero authors, compare against the empty string.
	var adl []int
	for _, aa := range orEmptyStringList(a.Author) {
		for _, ab := range orEmptyStringList(b.Author) {
			adl = append(adl, levenshtein.ComputeDistance(strings.ToLower(aa), strings.ToLower(ab)))
		}
	}
	if len(adl) == 0 {
		return 0
	}
	return min(adl...)
}

func distance(a, b *meta.Metadata) int {
	return titleDistance(a, b) + authorDistance(a, b)
}

type metaWithDistanceList []metaWithDistance

func (l metaWithDistanceList) Len() int           { return len(l) }
func (l metaWithDistanceList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l metaWithDistanceList) Less(i, j int) bool { return l[i].distance < l[j].distance }

// SelectWithConfidence returns one result out of a set of search
// results obtained via a call to Refiner.Similar() if it can
// establish with enough confidence that it matches what we searched
// for in 'ref'.
func SelectWithConfidence(ref *meta.Metadata, candidates []*meta.Metadata) (*meta.Metadata, bool) {
	if len(candidates) == 0 {
		return nil, false
	}

	// If we have a single result, we're pretty confident.
	if len(candidates) == 1 {
		return candidates[0], true
	}

	// Sort results by Levenshtein edit distance to the reference,
	// and return the first one if it's below our arbitrarily set
	// threshold. Consider the case of swapped author/title.
	swapRef := swapAuthorTitle(ref)
	md := make([]metaWithDistance, len(candidates))
	for i := 0; i < len(candidates); i++ {
		ad := authorDistance(ref, candidates[i])
		ads := authorDistance(swapRef, candidates[i])
		td := titleDistance(ref, candidates[i])
		tds := titleDistance(swapRef, candidates[i])
		md[i].authorDistance = min(ad, ads)
		md[i].titleDistance = min(td, tds)
		md[i].distance = min(ad+td, ads+tds)
		md[i].meta = candidates[i]

		var author string
		if len(candidates[i].Author) > 0 {
			author = candidates[i].Author[0]
		}
		log.Printf("candidate=%s/%s, distance=%d", candidates[i].Title, author, md[i].distance)
	}
	sort.Sort(metaWithDistanceList(md))

	// Compare the title and author distances to the threshold
	// separately, to allow larger corrections in the title OR the
	// author.
	if md[0].authorDistance <= editDistanceThreshold || md[0].titleDistance <= editDistanceThreshold {
		return md[0].meta, true
	}

	return nil, false
}

// Refine metadata by looking up stuff online.
func Refine(ctx context.Context, m *meta.Metadata, refiner Refiner) (*meta.Metadata, error) {
	// First, try to lookup explicit identifiers.
	results, err := refiner.Lookup(ctx, m.IDs)
	if err == nil {
		return results[0], nil
	}

	// Try a narrow search and see if we're confident with the result.
	results, err = refiner.Similar(ctx, m)
	if err != nil {
		return nil, err
	}

	if mm, ok := SelectWithConfidence(m, results); ok {
		return mm, nil
	}

	return nil, errors.New("no results found")
}
