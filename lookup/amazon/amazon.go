package amazon

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/cenkalti/backoff"
)

var (
	// Limit concurrency (limit is hard-coded).
	azSemaphore = make(chan struct{}, 3)

	// Shared http.Client.
	client http.Client
)

func doAPIRequest(uri string) (*http.Response, error) {
	azSemaphore <- struct{}{}
	defer func() {
		<-azSemaphore
	}()

	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Referer", "https://www.amazon.com/")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == 403 {
		resp.Body.Close()
		return nil, errors.New("ratelimited")
	}
	return resp, nil
}

func LookupISBN(isbn string) (string, error) {
	values := make(url.Values)
	values.Set("search-alias", "aps")
	values.Set("unfiltered", "1")
	values.Set("sort", "relevanceexprank")
	values.Set("field-isbn", isbn)

	uri := "https://www.amazon.com/s/?" + values.Encode()
	log.Printf("making request to %s", uri)

	var resp *http.Response
	err := backoff.Retry(func() (err error) {
		resp, err = doAPIRequest(uri)
		return
	}, backoff.NewExponentialBackOff())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	data, _ := ioutil.ReadAll(resp.Body)
	return string(data), nil
}
