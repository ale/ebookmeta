package google

import (
	"context"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"0xacab.org/ale/ebookmeta/lookup/httpclient"
	"0xacab.org/ale/ebookmeta/meta"
)

const googleBooksSourceName = "google"

var googleAPIKey = flag.String("google-api-key", "", "Google API key")

type Entry struct {
	GoogleID    string   `xml:"id"`
	Title       string   `xml:"http://purl.org/dc/terms title"`
	Date        string   `xml:"http://purl.org/dc/terms date"`
	Creator     []string `xml:"http://purl.org/dc/terms creator"`
	Description []string `xml:"http://purl.org/dc/terms description"`
	Language    []string `xml:"http://purl.org/dc/terms language"`
	Identifier  []string `xml:"http://purl.org/dc/terms identifier"`
	Publisher   []string `xml:"http://purl.org/dc/terms publisher"`
	Format      []string `xml:"http://purl.org/dc/terms format"`
	Subject     []string `xml:"http://purl.org/dc/terms subject"`

	Links []struct {
		Rel  string `xml:"rel,attr"`
		Href string `xml:"href,attr"`
		Type string `xml:"type,attr"`
	} `xml:"link"`
}

func (e *Entry) hasISBN() bool {
	for _, id := range e.Identifier {
		if strings.HasPrefix(id, "ISBN:") {
			return true
		}
	}
	return false
}

func (e *Entry) selfLink() string {
	for _, lnk := range e.Links {
		if lnk.Rel == "self" {
			return lnk.Href
		}
	}
	return ""
}

func (e *Entry) toMetadata(sourceQuality string) (*meta.Metadata, error) {
	var m meta.Metadata

	// Simple fields.
	m.Title = e.Title
	m.Author = e.Creator
	m.PublishedDate = meta.ParseDate(e.Date)
	m.Subject = e.Subject
	if len(e.Publisher) > 0 {
		m.Publisher = e.Publisher[0]
	}
	if len(e.Description) > 0 {
		m.Description = e.Description[0]
	}
	if len(e.Language) > 0 {
		m.Language = e.Language[0]
	}

	// Convert identifiers.
	for _, s := range e.Identifier {
		id, err := meta.ParseIdentifier(s, meta.IdTypeGoogle)
		if err != nil {
			return nil, err
		}
		m.IDs = append(m.IDs, id)
	}
	m.IDs = meta.UniqueIdentifiers(m.IDs)

	// Parse formats to find number of pages.
	for _, s := range e.Format {
		if strings.HasSuffix(s, " pages") {
			m.NumPages, _ = strconv.Atoi(s[:len(s)-6])
		} else {
			m.Format = append(m.Format, s)
		}
	}

	// Source information.
	m.Source = []meta.Source{
		{
			Name:    googleBooksSourceName,
			Quality: meta.SourceQuality(sourceQuality),
			URL:     e.selfLink(),
		},
	}

	return &m, nil
}

type feedResult struct {
	XMLName xml.Name `xml:"http://www.w3.org/2005/Atom feed"`
	Entries []*Entry `xml:"entry"`
}

func parseFeed(r io.Reader) ([]*Entry, error) {
	var result feedResult
	if err := xml.NewDecoder(r).Decode(&result); err != nil {
		return nil, err
	}
	return result.Entries, nil
}

type entryResult struct {
	XMLName xml.Name `xml:"http://www.w3.org/2005/Atom entry"`
	Entry
}

func parseEntry(r io.Reader) (*Entry, error) {
	var result entryResult
	if err := xml.NewDecoder(r).Decode(&result); err != nil {
		return nil, err
	}
	return &result.Entry, nil
}

func buildQueryForISBNs(isbns []string) string {
	query := make([]string, 0, len(isbns))
	for _, isbn := range isbns {
		query = append(query, fmt.Sprintf("isbn:%s", isbn))
	}
	return strings.Join(query, " OR ")
}

type googleBooksClient struct {
	httpclient.Client
}

func (gb *googleBooksClient) Get(ctx context.Context, uri string) (*http.Response, error) {
	// Add the API key transparently to all requests.
	if *googleAPIKey != "" {
		if strings.Contains(uri, "?") {
			uri += "&"
		} else {
			uri += "?"
		}
		uri += "key=" + *googleAPIKey
	}

	return gb.Client.Get(ctx, uri)
}

type GoogleBooks struct {
	client httpclient.Client
}

func New() *GoogleBooks {
	return &GoogleBooks{
		client: &googleBooksClient{Client: httpclient.New(3)},
	}
}

func (gb *GoogleBooks) lookupQuery(ctx context.Context, query string) ([]*Entry, error) {
	values := make(url.Values)
	values.Set("q", query)
	uri := "https://books.google.com/books/feeds/volumes?" + values.Encode()

	resp, err := gb.client.Get(ctx, uri)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return parseFeed(resp.Body)
}

func (gb *GoogleBooks) lookupEntry(ctx context.Context, uri string) (*Entry, error) {
	resp, err := gb.client.Get(ctx, uri)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return parseEntry(resp.Body)
}

func (gb *GoogleBooks) runQuery(ctx context.Context, query, sourceQuality string) ([]*meta.Metadata, error) {
	entries, err := gb.lookupQuery(ctx, query)
	if err != nil {
		return nil, err
	}

	out := make([]*meta.Metadata, 0, len(entries))
	for _, e := range entries {
		log.Printf("googlebooks: %+v", *e)

		// Sometimes we get an entry that does not have the
		// ISBN we asked for. In this case, try to look up the
		// self-link instead.
		if !e.hasISBN() {
			ee, err := gb.lookupEntry(ctx, e.selfLink())
			if err != nil {
				log.Printf("error looking up self link: %v", err)
			} else {
				e = ee
				log.Printf("googlebooks: replaced metadata with self link info: %+v", *e)
			}
		}

		m, err := e.toMetadata(sourceQuality)
		if err != nil {
			return nil, err
		}
		out = append(out, m)
	}
	return out, nil
}

func (gb *GoogleBooks) Lookup(ctx context.Context, identifiers []meta.Identifier) ([]*meta.Metadata, error) {
	var isbns []string
	for _, id := range identifiers {
		if id.Type == meta.IdTypeISBN {
			isbns = append(isbns, id.Value)
		}
	}
	if len(isbns) == 0 {
		return nil, errors.New("no ISBN identifiers found")
	}

	return gb.runQuery(ctx, buildQueryForISBNs(isbns), meta.SourceQualityISBN)
}

func (gb *GoogleBooks) Similar(ctx context.Context, m *meta.Metadata) ([]*meta.Metadata, error) {
	var query []string

	if m.Title != "" {
		query = append(query, fmt.Sprintf("intitle:\"%s\"", m.Title))
	}
	for _, a := range m.Author {
		query = append(query, fmt.Sprintf("inauthor:\"%s\"", a))
	}

	if len(query) == 0 {
		return nil, errors.New("no metadata to query")
	}

	return gb.runQuery(ctx, strings.Join(query, "+"), meta.SourceQualitySearch)
}
