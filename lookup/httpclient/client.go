package httpclient

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/cenkalti/backoff"
)

// Client is a generic HTTP client for a single domain or backend,
// with specific concurrency and rate limit controls.
type Client interface {
	Get(context.Context, string) (*http.Response, error)
}

type baseClient struct {
	sem    chan struct{}
	client *http.Client
}

// New returns a new Client with the specified maximum concurrency.
func New(concurrency int) Client {
	return &baseClient{
		sem:    make(chan struct{}, concurrency),
		client: new(http.Client),
	}
}

func (c *baseClient) Get(ctx context.Context, uri string) (resp *http.Response, err error) {
	b := backoff.WithContext(
		backoff.NewExponentialBackOff(), ctx)
	err = backoff.Retry(func() error {
		req, httpErr := http.NewRequest("GET", uri, nil)
		if httpErr != nil {
			return backoff.Permanent(httpErr)
		}

		c.sem <- struct{}{}
		defer func() {
			<-c.sem
		}()

		resp, httpErr = c.client.Do(req.WithContext(ctx))
		if httpErr != nil {
			return backoff.Permanent(httpErr)
		}
		if resp.StatusCode >= 500 || resp.StatusCode == 403 {
			resp.Body.Close()
			return fmt.Errorf("HTTP status %d", resp.StatusCode)
		}
		if resp.StatusCode != 200 {
			resp.Body.Close()
			return backoff.Permanent(fmt.Errorf("HTTP status %d", resp.StatusCode))
		}
		// The response Body is still open.
		return nil
	}, b)

	// Log the request.
	errStr := "OK"
	if err != nil {
		errStr = fmt.Sprintf("ERROR: %v", err)
	}
	log.Printf("HTTP request to %s -> %s", uri, errStr)

	return
}
