package google

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"0xacab.org/ale/ebookmeta/lookup/httpclient"
	"0xacab.org/ale/ebookmeta/meta"
)

const googleBooksSourceName = "google"

var googleAPIKey = flag.String("google-api-key", "", "Google API key")

type volume struct {
	GoogleID string      `json:"id"`
	Kind     string      `json:"kind"`
	SelfLink string      `json:"selfLink"`
	Info     *volumeInfo `json:"volumeInfo"`
}

type industryIdentifier struct {
	Type       string `json:"type"`
	Identifier string `json:"identifier"`
}

type volumeInfo struct {
	Title               string                `json:"title"`
	Authors             []string              `json:"authors"`
	Publisher           string                `json:"publisher"`
	PublishedDate       string                `json:"publishedDate"`
	Description         string                `json:"description"`
	IndustryIdentifiers []*industryIdentifier `json:"industryIdentifiers"`
	PageCount           int                   `json:"pageCount"`
	Categories          []string              `json:"categories"`
	ImageLinks          map[string]string     `json:"imageLinks"`
	Language            string                `json:"language"`
}

type searchResult struct {
	Items []*volume `json:"items"`
}

func (v *volume) hasISBN() bool {
	for _, id := range v.Info.IndustryIdentifiers {
		if id.Type == "ISBN_10" || id.Type == "ISBN_13" {
			return true
		}
	}
	return false
}

func (v *volume) toMetadata(sourceQuality string) *meta.Metadata {
	var m meta.Metadata

	// Simple fields.
	m.Title = v.Info.Title
	m.Author = v.Info.Authors
	m.Publisher = v.Info.Publisher
	m.PublishedDate = meta.ParseDate(v.Info.PublishedDate)
	m.Subject = v.Info.Categories
	m.Description = v.Info.Description
	m.Language = v.Info.Language
	m.NumPages = v.Info.PageCount
	m.Format = []string{v.Kind}

	// Convert identifiers.
	m.IDs = []meta.Identifier{
		{Type: meta.IdTypeGoogle, Value: v.GoogleID},
	}
	for _, iid := range v.Info.IndustryIdentifiers {
		id := meta.Identifier{Value: iid.Identifier}
		switch iid.Type {
		case "ISBN", "ISBN_10", "ISBN_13":
			id.Type = meta.IdTypeISBN
		default:
			id.Type = strings.ToLower(iid.Type)
		}
		m.IDs = append(m.IDs, id)
	}

	// Source information.
	m.Source = []meta.Source{
		{
			Name:      googleBooksSourceName,
			Quality:   meta.SourceQuality(sourceQuality),
			URL:       v.SelfLink,
			Timestamp: time.Now(),
		},
	}

	return m.Normalize()
}

func parseVolume(r io.Reader) (*volume, error) {
	var result volume
	if err := json.NewDecoder(r).Decode(&result); err != nil {
		return nil, err
	}
	return &result, nil
}

func parseFeed(r io.Reader) ([]*volume, error) {
	var result searchResult
	if err := json.NewDecoder(r).Decode(&result); err != nil {
		return nil, err
	}
	return result.Items, nil
}

func buildQueryForISBNs(isbns []string) string {
	query := make([]string, 0, len(isbns))
	for _, isbn := range isbns {
		query = append(query, fmt.Sprintf("isbn:%s", isbn))
	}
	return strings.Join(query, " OR ")
}

type googleBooksClient struct {
	httpclient.Client
}

func (gb *googleBooksClient) Get(ctx context.Context, uri string) (*http.Response, error) {
	// Add the API key transparently to all requests.
	if *googleAPIKey != "" {
		if strings.Contains(uri, "?") {
			uri += "&"
		} else {
			uri += "?"
		}
		uri += "key=" + *googleAPIKey
	}

	return gb.Client.Get(ctx, uri)
}

type GoogleBooks struct {
	client httpclient.Client
}

func New() *GoogleBooks {
	return &GoogleBooks{
		client: &googleBooksClient{Client: httpclient.New(3)},
	}
}

func (gb *GoogleBooks) lookupVolume(ctx context.Context, uri string) (*volume, error) {
	resp, err := gb.client.Get(ctx, uri)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return parseVolume(resp.Body)
}

func (gb *GoogleBooks) lookupQuery(ctx context.Context, query string) ([]*volume, error) {
	values := make(url.Values)
	values.Set("q", query)
	uri := "https://www.googleapis.com/books/v1/volumes?" + values.Encode()

	resp, err := gb.client.Get(ctx, uri)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return parseFeed(resp.Body)
}

func (gb *GoogleBooks) runQuery(ctx context.Context, query, sourceQuality string) ([]*meta.Metadata, error) {
	volumes, err := gb.lookupQuery(ctx, query)
	if err != nil {
		return nil, err
	}

	out := make([]*meta.Metadata, 0, len(volumes))
	for _, v := range volumes {
		log.Printf("googlebooks: %+v", *v)

		// Sometimes we get results without an ISBN, try to
		// look up the entry directly in that case.
		if !v.hasISBN() {
			v, err = gb.lookupVolume(ctx, v.SelfLink)
			if err != nil {
				return nil, err
			}
		}

		out = append(out, v.toMetadata(sourceQuality))
	}
	return out, nil
}

func (gb *GoogleBooks) Lookup(ctx context.Context, identifiers []meta.Identifier) ([]*meta.Metadata, error) {
	var isbns []string
	for _, id := range identifiers {
		if id.Type == meta.IdTypeISBN {
			isbns = append(isbns, id.Value)
		}
	}
	if len(isbns) == 0 {
		return nil, errors.New("no ISBN identifiers found")
	}

	return gb.runQuery(ctx, buildQueryForISBNs(isbns), meta.SourceQualityISBN)
}

func authorTitleQuery(title string, authors []string) string {
	var query []string
	if title != "" {
		query = append(query, fmt.Sprintf("intitle:\"%s\"", strings.ToLower(removeSpecialChars(title))))
	}
	for _, a := range authors {
		query = append(query, fmt.Sprintf("inauthor:\"%s\"", strings.ToLower(removeSpecialChars(a))))
	}
	return strings.Join(query, " AND ")
}

func genericQuery(title string, authors []string) string {
	var query []string
	if title != "" {
		query = append(query, strings.ToLower(removeSpecialChars(title)))
	}
	for _, a := range authors {
		query = append(query, strings.ToLower(removeSpecialChars(a)))
	}
	return strings.Join(query, " ")
}

func removeSpecialChars(s string) string {
	return strings.Map(func(c rune) rune {
		switch c {
		case '"', ':', '(', ')':
			return -1
		}
		return c
	}, s)
}

func (gb *GoogleBooks) Similar(ctx context.Context, m *meta.Metadata) ([]*meta.Metadata, error) {
	queries := []string{
		authorTitleQuery(m.Title, m.Author),
		genericQuery(m.Title, m.Author),
	}

	for _, q := range queries {
		if q == "" {
			continue
		}
		results, err := gb.runQuery(ctx, q, meta.SourceQualitySearch)
		if err != nil {
			return nil, err
		}
		if len(results) > 0 {
			return results, nil
		}
	}

	return nil, errors.New("no metadata to query")
}
