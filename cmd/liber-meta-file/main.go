package main

import (
	"flag"
	"io"
	"log"
	"os"

	"0xacab.org/ale/ebookmeta/detect"
	"0xacab.org/ale/ebookmeta/meta"
	"0xacab.org/ale/ebookmeta/rec"
)

var (
	outputPath = flag.String("output", "-", "output file (- for stdout)")
	//errorPath  = flag.String("errout", "-", "errors file (- for stderr)")
)

func openOutput() (io.Writer, error) {
	if *outputPath == "-" {
		return os.Stdout, nil
	}
	return os.Create(*outputPath)
}

type inputRecord struct {
	Path string `json:"path"`
}

type outputRecord struct {
	Path     string         `json:"path"`
	Metadata *meta.Metadata `json:"meta"`
}

func processRecord(r *inputRecord) (*outputRecord, error) {
	m, err := detect.ParseMetadataFromFile(r.Path)
	if err != nil {
		return nil, err
	}

	return &outputRecord{
		Path:     r.Path,
		Metadata: m,
	}, nil
}

func main() {
	flag.Parse()

	input := rec.NewReader(os.Stdin)
	outf, err := openOutput()
	if err != nil {
		log.Fatal(err)
	}
	output := rec.NewWriter(outf)

	for {
		var r inputRecord
		if err := input.ReadRecord(&r); err != nil {
			break
		}

		o, err := processRecord(&r)
		if err != nil {
			log.Printf("error processing record: %v", err)
			continue
		}

		if err := output.WriteRecord(o); err != nil {
			log.Fatalf("output error: %v", err)
		}
	}
}
