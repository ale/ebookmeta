package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"
	"time"

	"0xacab.org/ale/ebookmeta/lookup"
	"0xacab.org/ale/ebookmeta/meta"
	"0xacab.org/ale/ebookmeta/rec"
)

var (
	be         = flag.String("backend", "google", "which backend to use")
	outputPath = flag.String("output", "-", "output file (- for stdout)")
	//errorPath  = flag.String("errout", "-", "errors file (- for stderr)")
)

func openOutput() (io.Writer, error) {
	if *outputPath == "-" {
		return os.Stdout, nil
	}
	return os.Create(*outputPath)
}

type inputRecord struct {
	Path     string         `json:"path"`
	Metadata *meta.Metadata `json:"meta"`
}

type outputRecord inputRecord

var refineTimeout = 120 * time.Second

func processRecord(r *inputRecord, ref lookup.Refiner) (*outputRecord, error) {
	ctx, cancel := context.WithTimeout(context.Background(), refineTimeout)
	defer cancel()

	m, err := lookup.Refine(ctx, r.Metadata, ref)
	if err != nil {
		log.Printf("Refine() error: %v", err)
		return nil, err
	}

	m = r.Metadata.Merge(m)

	return &outputRecord{
		Path:     r.Path,
		Metadata: m,
	}, nil
}

func main() {
	flag.Parse()

	ref, err := lookup.New(*be)
	if err != nil {
		log.Fatal(err)
	}

	input := rec.NewReader(os.Stdin)
	outf, err := openOutput()
	if err != nil {
		log.Fatal(err)
	}
	output := rec.NewWriter(outf)

	for {
		var r inputRecord
		if err := input.ReadRecord(&r); err != nil {
			break
		}

		o, err := processRecord(&r, ref)
		if err != nil {
			log.Printf("error processing record: %v", err)
			continue
		}

		if err := output.WriteRecord(o); err != nil {
			log.Fatalf("output error: %v", err)
		}
	}
}
