package corrections

import (
	"compress/bzip2"
	"compress/gzip"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strings"

	"github.com/ulikunitz/xz"
)

type Dataset struct {
	path string
	data map[string]string
}

// Automatically decompress files based on their extension.
func decompressOnTheFly(path string, r io.Reader) (io.Reader, error) {
	var err error
	switch {
	case strings.HasSuffix(path, ".gz"):
		r, err = gzip.NewReader(r)
	case strings.HasSuffix(path, ".bz2"):
		r = bzip2.NewReader(r)
	case strings.HasSuffix(path, ".xz"):
		r, err = xz.NewReader(r)
	}
	return r, err
}

func LoadDataset(path string) (*Dataset, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	r, err := decompressOnTheFly(path, f)
	if err != nil {
		return nil, err
	}

	return loadDatasetFromReader(path, r)
}

func loadDatasetFromReader(path string, r io.Reader) (*Dataset, error) {
	csvr := csv.NewReader(r)
	csvr.FieldsPerRecord = 2
	csvr.ReuseRecord = true

	data := make(map[string]string)

	for {
		rec, err := csvr.Read()
		switch err {
		case nil:
		case csv.ErrFieldCount:
			continue
		case io.EOF:
			log.Printf("dataset: loaded %d records from %s", len(data), path)
			return &Dataset{path: path, data: data}, nil
		default:
			return nil, err
		}
		data[rec[0]] = rec[1]
	}
}

func (d *Dataset) Lookup(key string) (string, bool) {
	value, ok := d.data[key]
	return value, ok
}
