package rec

import (
	"bufio"
	"encoding/json"
	"io"
)

type Reader struct {
	*bufio.Scanner
}

func NewReader(r io.Reader) *Reader {
	return &Reader{bufio.NewScanner(r)}
}

func (r *Reader) ReadRecord(out interface{}) error {
	if !r.Scanner.Scan() {
		err := r.Scanner.Err()
		if err != nil {
			return err
		}
		return io.EOF
	}
	return json.Unmarshal(r.Scanner.Bytes(), out)
}

type Writer struct {
	io.Writer
}

func NewWriter(w io.Writer) *Writer {
	return &Writer{w}
}

func (w *Writer) WriteRecord(obj interface{}) error {
	return json.NewEncoder(w).Encode(obj)
}
