package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"0xacab.org/ale/ebookmeta/detect"
)

func main() {
	flag.Parse()

	path := flag.Arg(0)
	m, err := detect.ParseMetadataFromFile(path)
	if err != nil {
		log.Fatal(err)
	}

	// Print.
	s, _ := json.MarshalIndent(m, "", "  ")
	fmt.Printf("%s\n", string(s))
}
